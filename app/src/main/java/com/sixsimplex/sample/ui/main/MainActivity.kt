package com.sixsimplex.sample.ui.main

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.esri.arcgisruntime.ArcGISRuntimeEnvironment
import com.esri.arcgisruntime.data.ServiceFeatureTable
import com.esri.arcgisruntime.layers.FeatureLayer
import com.esri.arcgisruntime.loadable.LoadStatus
import com.esri.arcgisruntime.mapping.ArcGISMap
import com.esri.arcgisruntime.mapping.Basemap
import com.esri.arcgisruntime.portal.Portal
import com.esri.arcgisruntime.portal.PortalItem
import com.esri.basicandroidproject.R
import com.esri.basicandroidproject.databinding.ActivityMainBinding
import com.sixsimplex.sample.ui.base.BaseActivity
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity<ActivityMainBinding>() {

    companion object {
        // Trail heads layer id taken from sample.
        const val itemID = "2e4b3df6ba4b44969a3bc9827de746b3"
    }

    @LayoutRes
    override fun getLayoutResId() = R.layout.activity_main

    /**
     * ViewModel injected by DI.
     */
    private val viewModel by viewModel<MainViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupMap()
        addTrailHeadLayer()
    }

    /**
     * Initialize the map.
     */
    private fun setupMap() {
        ArcGISRuntimeEnvironment.setLicense(resources.getString(R.string.arcgis_license_key))
        val basemapType = Basemap.Type.TOPOGRAPHIC
        val latitude = 34.0270
        val longitude = -118.8050
        val levelOfDetail = 13
        val map = ArcGISMap(basemapType, latitude, longitude, levelOfDetail)

        binding.mapView.map = map
        addLayer(map)
    }

    /**
     * Add trail head layer on map.
     */
    private fun addTrailHeadLayer() {
        val url = "https://services3.arcgis.com/GVgbJbqm8hXASVYi/arcgis/rest/services/Trailheads/FeatureServer/0"
        val serviceFeatureTable = ServiceFeatureTable(url)
        val featureLayer = FeatureLayer(serviceFeatureTable)
        binding.mapView.map.operationalLayers.add(featureLayer)
    }

    /**
     * Add ArcGis map feature layer on map.
     */
    private fun addLayer(map: ArcGISMap) {
        val portal = Portal("http://www.arcgis.com")
        val portalItem = PortalItem(portal, itemID)
        val featureLayer = FeatureLayer(portalItem, 0)
        featureLayer.addDoneLoadingListener {
            if (featureLayer.loadStatus == LoadStatus.LOADED) {
                map.operationalLayers.add(featureLayer);
            }
        }
        featureLayer.loadAsync()
    }

    override fun onPause() {
        binding.mapView.pause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.resume()
    }

    override fun onDestroy() {
        binding.mapView.dispose()
        super.onDestroy()
    }
}