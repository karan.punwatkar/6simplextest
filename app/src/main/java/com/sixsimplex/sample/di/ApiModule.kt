package com.sixsimplex.sample.di

import com.sixsimplex.sample.network.ApiService
import org.koin.dsl.module.module
import retrofit2.Retrofit

val apiModule = module {
    single(createOnStart = false) { get<Retrofit>().create(ApiService::class.java) }
}