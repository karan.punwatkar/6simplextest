package com.sixsimplex.sample

import android.app.Application
import com.sixsimplex.sample.di.*
import org.koin.android.ext.android.startKoin


class AppApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(
                apiModule,
                remoteDataModule,
                repositoryModule,
                viewModelModule))
    }
}