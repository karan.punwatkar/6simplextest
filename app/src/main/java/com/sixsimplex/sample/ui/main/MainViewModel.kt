package com.sixsimplex.sample.ui.main

import com.sixsimplex.sample.repository.MainRepository
import com.sixsimplex.sample.ui.base.BaseViewModel


class MainViewModel(private val repository: MainRepository) : BaseViewModel() {

    companion object {
        private const val TAG = "MainViewModel"
    }
}