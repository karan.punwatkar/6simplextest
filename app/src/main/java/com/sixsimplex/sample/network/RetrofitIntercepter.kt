package com.sixsimplex.sample.network

import android.util.Log
import okhttp3.Interceptor
import okhttp3.Response

class RetrofitInterceptor : Interceptor {

    companion object{
        private final var TAG = RetrofitInterceptor::class.java.simpleName
    }

    override fun intercept(chain: Interceptor.Chain?): Response {
        val request = chain?.request()
        val newRequest = request?.newBuilder()

        try {
            newRequest?.addHeader("Accept", "application/json")
        } catch (ex: Throwable) {
            Log.e(TAG, "Auth Error")
        }

        return chain?.proceed(newRequest!!.build())!!
    }
}