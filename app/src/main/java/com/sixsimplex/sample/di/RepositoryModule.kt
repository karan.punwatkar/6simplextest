package com.sixsimplex.sample.di

import com.sixsimplex.sample.repository.MainRepository
import org.koin.dsl.module.module

val repositoryModule = module {
    factory { MainRepository(get()) }
}